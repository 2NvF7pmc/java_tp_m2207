package com.m2207.tp5;

import javax.swing.*;
import java.awt.*;

public class MonAppli
{
    public static void main(String[] args)
    {
        //Ex 1.1
        MonAppliGraphique _app = new MonAppliGraphique();

        //Ex 4.1
        CompteurDeClic _compteurClic = new CompteurDeClic();

        //Ex 5.1
        PlusOuMoinsCher _plusOuMoin = new PlusOuMoinsCher();
    }
}
