package com.m2207.tp5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlusOuMoinsCher extends JFrame implements ActionListener
{
    //Ex 5.1
    private JLabel LabelTextField;
    private JTextField TextField;
    private JButton CheckBtn;
    private JLabel LabelResponse;

    //Ex 5.3
    private int NbrDevine;

    //Ex 5.6
    private int NbrCoups = 0;

    public PlusOuMoinsCher ()
    {
        super();
        setTitle("Plus cher ou moins cher ?");
        setSize(300, 150);
        setLocation(300, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        Container _container = getContentPane();
        _container.setLayout(new GridLayout(2,2));

        //Ex 5.1
        LabelTextField = new JLabel("Votre proposition :");
        _container.add(LabelTextField);
        TextField = new JTextField();
        _container.add(TextField);
        CheckBtn = new JButton("Vérifie !");
        _container.add(CheckBtn);
        LabelResponse = new JLabel();
        _container.add(LabelResponse);

        //Ex 5.2
        CheckBtn.addActionListener(this);

        Init();
        setVisible(true);
    }

    public void actionPerformed(ActionEvent actionEvent)
    {
        int _value = Integer.parseInt(TextField.getText());
        System.out.printf("Contenue dans le champ de saisie : %d\n", _value);

        //Ex 5.4
        if(_value == NbrDevine)
        {
            LabelResponse.setText("Vous avez deviné !");
            Init ();
        }
        else if (_value < NbrDevine) LabelResponse.setText("Plus cher !");
        else LabelResponse.setText("Moin cher !");

        //Ex 5.6
        NbrCoups++;
        if(NbrCoups == 8)
        {
            System.out.println("Nombre de coup max atteint.");
            NbrCoups = 0;
            Init();
        }
    }

    //Ex 5.5
    private void Init ()
    {
        NbrDevine = getIntRand(0,100);
        System.out.printf("Nombre à deviner %d.\n", NbrDevine);

        LabelResponse.setText("");
        TextField.setText("");
    }

    public int getIntRand (int _min, int _max)
    {
        return _min  + (int) (Math.random() * ((_max - _min) + 1));
    }
}
