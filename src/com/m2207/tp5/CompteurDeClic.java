package com.m2207.tp5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CompteurDeClic extends JFrame implements ActionListener
{
    private static JButton ClickBtn;
    private static JLabel ClickLabel;
    private static int NbrClick = 0;

    public  CompteurDeClic ()
    {
        super();
        setTitle("Compteur de clic");
        setSize(200, 100);
        setLocation(20, 20);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Ex 4.1
        Container _container = getContentPane();
        _container.setLayout(new FlowLayout());
        ClickBtn = new JButton("Click !");
        ClickLabel = new JLabel("Vous avez cliqué "+ NbrClick +" fois");
        _container.add(ClickBtn);
        _container.add(ClickLabel);

        //Ex 4.2
        ClickBtn.addActionListener(this);

        setVisible(true);
    }

    //Ex 4.2
    public void actionPerformed(ActionEvent actionEvent)
    {
        System.out.println("Une action a été détectée");

        //Ex 4.2
        NbrClick++;
        ClickLabel.setText("Vous avez cliqué "+ NbrClick +" fois");
    }
}
