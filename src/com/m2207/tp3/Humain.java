package com.m2207.tp3;

public class Humain
{
    protected String Nom, Boisson;

    public Humain (String _nom)
    {
        Nom = _nom;
        Boisson = "lait";
    }

    public String QuelEstTonNom ()
    {
        return Nom;
    }

    public String QuelleEstTaBoisson ()
    {
        return Boisson;
    }

    public void Parler (String _texte)
    {
        System.out.printf("(%s) - %s\n", Nom, _texte);
    }

    public void SePresenter ()
    {
        Parler("Bonjour, je suis " +
                QuelEstTonNom() +
                " et ma boisson préférée est le " +
                QuelleEstTaBoisson() + ".");
    }

    public void Boire ()
    {
        Parler("Ah ! un bon verre de " + QuelleEstTaBoisson() + "! GLOUPS !");
    }
}
