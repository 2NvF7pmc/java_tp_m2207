package com.m2207.tp3;

public class Cowboy extends Humain
{
    private int Populariter;
    private String Caracteristique;

    public Cowboy (String _nom)
    {
        super(_nom);
        Boisson = "whiskey";
        Populariter = 0;
        Caracteristique = "vaillant";
    }

    public void SePresenter ()
    {
        super.SePresenter();
        Parler("Je suis " + Caracteristique + " et ma popularité est " + Populariter);
    }

    public void Tire (Brigand _brigand)
    {
        System.out.printf("Le %s %s tire sur %s. PAN !\n",
                Caracteristique,
                Nom,
                _brigand.Nom);
        Parler("Prend ça, voyou !");
    }

    public void Libere (Dame _dame)
    {
        _dame.EstLiberee();
        Populariter += 10;
    }
}
