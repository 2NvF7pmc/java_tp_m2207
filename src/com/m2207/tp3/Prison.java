package com.m2207.tp3;

import java.util.Vector;

public class Prison
{
    private String Nom;

    //EX 12
    //private int NbrTotalPrisioner;
    private int NbrTotalPrisioner () { return (Prisionniers != null) ? Prisionniers.size() : 0;}

    //EX 12
    //private Brigand[] Prisionniers;
    private Vector<Brigand> Prisionniers;

    public Prison (String _nom)
    {
        Nom = _nom;
        //EX 12
        //NbrTotalPrisioner = 10;
        //Prisionniers = new Brigand[NbrTotalPrisioner];
        Prisionniers = new Vector<Brigand>();
    }

    public void MettreEnCellule (Brigand _b)
    {
        //EX 12
        /*for (int i = 0; i < NbrTotalPrisioner; i++)
            if(Prisionniers[i] == null)
            {
                System.out.printf("%s le %s est mis dans la cellule n°%d\n",
                        _b.Nom,
                        _b.getLook(),
                        i);
                Prisionniers[i] = _b;
                break;
            }*/
        Prisionniers.add(_b);
        System.out.printf("%s le %s est mis dans la cellule n°%d\n",
                _b.Nom,
                _b.getLook(),
                NbrTotalPrisioner());
    }

    public void SortirDeCellule (Brigand _b)
    {
        //EX 12
        /*for (int i = 0; i < NbrTotalPrisioner; i++)
            if(Prisionniers[i] != null && Prisionniers[i] == _b)
            {
                System.out.printf("%s le %s est sorti de sa cellule\n",
                        _b.Nom,
                        _b.getLook());
                Prisionniers[i] = null;
            }*/
        for (int i = 0; i < NbrTotalPrisioner(); i++)
        {
            if(Prisionniers.get(i) != null && Prisionniers.get(i) == _b)
            {
                System.out.printf("%s le %s est sorti de sa cellule\n",
                        _b.Nom,
                        _b.getLook());
                Prisionniers.remove(i);
            }

        }
    }
    
    public void CompterLesPrisonniers ()
    {
        //EX 12
        /*int _r = 0;
        for (Brigand _b:Prisionniers) if(_b != null) _r++;
        System.out.printf("Il y a %d brigand(s) dans la prison %s\n",
                _r,
                Nom);*/
        System.out.printf("Il y a %d brigand(s) dans la prison %s\n",
                NbrTotalPrisioner(),
                Nom);
    }
}
