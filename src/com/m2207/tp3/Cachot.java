package com.m2207.tp3;

public class Cachot
{
    private String Nom;
    private int NbrTotalPrisioner;
    private Humain[] Prisionniers;

    public Cachot (String _nom)
    {
        Nom = _nom;
        NbrTotalPrisioner = 10;
        Prisionniers = new Humain[NbrTotalPrisioner];
    }

    public void MettreEnCellule (Humain _b)
    {
        for (int i = 0; i < NbrTotalPrisioner; i++)
            if(Prisionniers[i] == null)
            {
                System.out.printf("%s est mis dans le cachot n°%d\n",
                        _b.Nom,
                        i);
                Prisionniers[i] = _b;
                break;
            }
    }

    public void SortirDeCellule (Humain _b)
    {
        for (int i = 0; i < NbrTotalPrisioner; i++)
            if(Prisionniers[i] != null && Prisionniers[i] == _b)
            {
                System.out.printf("%s est sorti de sont cachot\n",
                        _b.Nom);
                Prisionniers[i] = null;
            }
    }

    public void CompterLesPrisonniers ()
    {
        int _r = 0;
        for (Humain _h : Prisionniers) if(_h != null) _r++;
        System.out.printf("Il y a %d personnes dans le cachot %s\n",
                _r,
                Nom);
    }
}
