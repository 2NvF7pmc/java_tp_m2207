package com.m2207.tp3;

public class Dame extends Humain
{
    private boolean Libre;

    public Dame (String _nom)
    {
        super(_nom);
        Boisson = "Martini";
        Libre = true;
    }

    public void PriseEnOtage ()
    {
        Libre = false;
        Parler("Au secours !");
    }

    public void EstLiberee ()
    {
        Libre = true;
        Parler("Merci Cowboy");
    }

    public String QuelEstTonNom()
    {
        return "Miss "+Nom;
    }

    public void SePresenter()
    {
        super.SePresenter();
        Parler("Actuellement, je suis " + ((Libre) ? "libre" : "kidnappée"));
    }
}
