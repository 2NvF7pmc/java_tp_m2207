package com.m2207.tp3;

public class Brigand extends Humain
{
    private String Look;
    private int NbrDameKidnap, Recompense;

    private boolean EnPrison;

    public int getRecompense() { return Recompense; }

    public String getLook() { return Look; }

    public Brigand (String _nom)
    {
        super(_nom);
        Look = "méchant";
        NbrDameKidnap = 0;
        Recompense = 100;
        EnPrison = false;
        Boisson = "cognac";
    }

    public String QuelEstTonNom()
    {
        return Nom+" le " + Look;
    }

    public void SePresenter()
    {
        super.SePresenter();
        Parler("J'ai l'air " + Look + " et j'ai enlevé "+NbrDameKidnap+" dames");
        Parler("Ma tête est mise à prix " + Recompense +"$ !!");
    }

    public void Enleve (Dame _dame)
    {
        _dame.PriseEnOtage();
        NbrDameKidnap++;
        Recompense += 100;
        Parler("Ah ah ! " + _dame.Nom +", tu es ma prisonnière !");
    }

    public void Emprisonner (Sherif _s)
    {
        Parler("Damned, je suis fait ! " + _s.Nom +", tu m'as eu !");
    }
}
