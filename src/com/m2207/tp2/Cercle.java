package com.m2207.tp2;
import java.lang.Math;

public class Cercle extends Forme
{
	private double Rayon;
	
	public double getRayon () { return Rayon; }
	public void setRayon (double _r) { Rayon = _r; }
	
	public Cercle ()
	{
		super();
		Rayon = 1.0;
	}
	
	public Cercle (double _r)
	{
		super();
		Rayon = _r;
	}
	
	public Cercle (double _r, String _couleur, boolean _coloriage)
	{
		super(_couleur, _coloriage);
		Rayon = _r;
	}
	
	public String seDecrire ()
	{
		return "Un Cercle de rayon " + Rayon +" est issue d�" + super.seDecrire().toLowerCase();
	}
	
	public double CalculerAire ()
	{
		return Math.PI * Math.pow(Rayon, 2);
	}
	
	public double CalculerPerimetre ()
	{
		return 2 * Math.PI * Rayon;
	}

}
