package com.m2207.tp2;

public class Forme
{
	private String Couleur;
	private boolean Coloriage;
	
	private static int NbrObject = 0;
	
	public String getCouleur () { return Couleur; }
	public void setCouleur (String _c) { Couleur = _c; }
	
	public boolean isColoriage () { return Coloriage; }
	public void setColoriage (boolean _b) { Coloriage = _b; }
	
	public static int getNbrObject () { return NbrObject; }
	
	
	public Forme ()
	{
		Couleur = "orange";
		Coloriage = true;
		
		NbrObject++;
	}
	
	public Forme (String _c, boolean _r)
	{
		Couleur = _c;
		Coloriage = _r;
		
		NbrObject++;
	}
	
	public String seDecrire ()
	{
		return "Une Forme de couleur " + Couleur + " et de coloriage " + Coloriage + ".\n";
	}
}
