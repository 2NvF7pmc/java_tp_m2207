package com.m2207.tp2;

public class TestForme
{
	public static void main(String[] args)
	{
		//Ex 4.1
		System.out.println(Forme.getNbrObject() + " objets de type Forme ont d�j� �t� cr��s");
		
		//Ex 1.4
		Forme f1 = new Forme ();
		Forme f2 = new Forme ("vert", false);
		System.out.printf("f1 : %s - %b\n",
				f1.getCouleur(),
				f1.isColoriage());
		System.out.printf("f2 : %s - %b\n",
				f2.getCouleur(),
				f2.isColoriage());
		
		//Ex 1.5
		f1.setCouleur("rouge");
		f1.setColoriage(false);
		System.out.printf("f1 : %s - %b\n",
				f1.getCouleur(),
				f1.isColoriage());
		
		//Ex 1.7
		System.out.printf(f1.seDecrire());
		System.out.printf(f2.seDecrire());
		
		//Ex 2.3 & 2.5
		Cercle c1 = new Cercle ();
		System.out.printf(c1.seDecrire());
		
		
		//Ex 2.7
		//Ex 2.8, par default lord de intanciation le constructeur par defaut sera appeler.
		Cercle c2 = new Cercle (2.5);
		System.out.printf(c2.seDecrire());
		
		//Ex 2.10
		Cercle c3 = new Cercle (3.2, "jaune", false);
		System.out.printf(c3.seDecrire());
		
		//Ex 2.11
		System.out.printf("C2 - aire : %.2f - perimetre : %.2f\n",
				c2.CalculerAire(),
				c2.CalculerPerimetre());
		
		System.out.printf("C3 - aire : %.2f - perimetre : %.2f\n",
				c3.CalculerAire(),
				c3.CalculerPerimetre());
		
		//Ex 3.2
		Cylindre cy1 = new Cylindre ();
		System.out.printf(cy1.seDecrire());
		
		//Ex 3.4
		Cylindre cy2 = new Cylindre (4.2, 1.3,"bleu", true);
		System.out.printf(cy2.seDecrire());
		
		//Ex 3.5
		System.out.printf("cy1 - volume : %.2f\n", cy1.CalculerVolume());
		System.out.printf("cy2 - volume : %.2f\n", cy2.CalculerVolume());
		
		//Ex 4.1
		System.out.println(Forme.getNbrObject() + " objets de type Forme ont d�j� �t� cr��s");
	}
}
