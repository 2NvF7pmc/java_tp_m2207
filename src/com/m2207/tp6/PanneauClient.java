package com.m2207.tp6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.net.Socket;

public class PanneauClient extends JFrame implements ActionListener
{
    protected JTextField TextField;
    protected JButton ExitButton;

    protected Socket monSocket;
    protected PrintWriter monPrintWriter;

    public static void main(String[] args)
    {
        //Ex 6.1
        new PanneauClient();
    }

    //EX 6.1
    public PanneauClient ()
    {
        super();
        setTitle("Client - Panneau d'affichage");
        setSize(250,120);
        setLocation(20,20);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container _panneau = getContentPane();
        _panneau.setLayout(new BorderLayout());
        TextField = new JTextField();
        ExitButton = new JButton("Envoyer");
        _panneau.add(TextField, BorderLayout.NORTH);
        _panneau.add(ExitButton, BorderLayout.SOUTH);

        ExitButton.addActionListener(this);

        setVisible(true);

        //EX 6.3
        try
        {
            monSocket = new Socket("localhost",8888);
            monPrintWriter = new PrintWriter(monSocket.getOutputStream());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent actionEvent)
    {
        //Ex 6.2
        emettre();
    }

    //Ex 6.2
    private void emettre ()
    {
        System.out.println("Envoi d'un message");

        //EX 6.4
        String msg = TextField.getText();
        if(msg.length() > 0)
        {
            TextField.setText("");
            monPrintWriter.println(msg);
            monPrintWriter.flush();
        }
    }
}
