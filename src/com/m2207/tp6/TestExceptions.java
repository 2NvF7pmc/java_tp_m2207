package com.m2207.tp6;

public class TestExceptions
{
    public static void main(String[] args)
    {
        //EX 1.1
        // Que se passe-t-il lors de l’exécution ?
        // Le programe initilise les variables, puis nous
        // afficher le resultat du calcule x/y et genere une errer
        //------------------------------------------------
        // Pourquoi ?
        // Mais nous savons qu'une division par 0 n'est pas possible
        // en math d'oul'errerjava.lang.ArithmeticException
        //-----------------------------------------------------
        // Arrivez-vous à voir le message «Fin de programme» dans la console ?
        // Non

        int x = 2, y = 0;

        //EX 1.2
        //------------------------------------------
        // Que constatez-vous maintenant ?
        // Le programme compile sens erreur et l'exception
        // et géré par la try...catch

        //Ex 1.3
        //-------------------------------------------
        //Que constatez-vous sur l’exécution de l’ensemble des instructionsdu programme ?
        //seul le premier bloc d'instruction "System.out.println("y/x = " + y/x);"
        //est executer puis une exception et lever par la catch suite a l'instruction suivant
        // Par conséquance la suit des instructions dans le try n'est pas exécuter
        try
        {
            //System.out.println(x/y);

            //Ex 1.3
            System.out.println("y/x = " + y/x);
            System.out.println("x/y = " + x/y);
            System.out.println("Commande de fermeture du programme");
        }
        catch (Exception e)
        {
            System.out.println("Une exception a été capturée");
        }
        System.out.println("Fin du programme");
    }
}
