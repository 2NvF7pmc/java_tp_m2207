package com.m2207.tp6;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class MonServeur
{
    public static void main(String[] args)
    {
        //Ex 3.1
        ServerSocket monServerSocket;
        Socket monSocketClient;
        BufferedReader monBufferedReader;
        try
        {
            //Ex 3.1
            monServerSocket = new ServerSocket(8888);
            System.out.printf("Server socket: %s\n", monServerSocket);

            //Ex 3.2 (activer Telnet sur window 'pkgmgr/iu:"TelnetClient"')
            monSocketClient = monServerSocket.accept();
            System.out.println("Le client s'est connecté");

            //Ex 3.3
            monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream()));
            System.out.printf("Message : %s\n", monBufferedReader.readLine());

            monSocketClient.close();
            monServerSocket.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
