package com.m2207.tp1;

public class MaBanque
{
	public static void main(String[] args)
	{
		//Ex 2.3
		Compte _myAccount = new Compte(1);
		System.out.printf("Montant du d�couvert autoris� : %.1f\n", _myAccount.getDecouvert());
		_myAccount.setDecouvert(100);
		System.out.printf("Montant du d�couvert autoris� : %.1f\n", _myAccount.getDecouvert());
		
		//Ex 2.4
		_myAccount.AfficherSolde();
		_myAccount.Depot(1000);
		_myAccount.AfficherSolde();
		
		//Ex 2.6
		Compte _c2 = new Compte(2);
		_c2.Depot(1000);
		_c2.AfficherSolde();
		System.out.printf("%s Nouveau Solde : %.1f\n",
				_c2.Retrait(600),
				_c2.getSolde());
		System.out.printf("%s Nouveau Solde : %.1f\n",
				_c2.Retrait(700),
				_c2.getSolde());
		_c2.setDecouvert(500);
		System.out.printf("%s Nouveau Solde : %.1f\n",
				_c2.Retrait(700),
				_c2.getSolde());
		
		//Ex 3.4
		Client _client1 = new Client ("Pierre","Poivre",_c2);
		_client1.AfficherSolde();
			
		//Ex 4.4
		System.out.println("------ S�nario de l'exercice 4.4 ------");	
		Compte _accountTen = new Compte(10);
		_accountTen.Depot(1000);
		ClientMultiComptes _client = new ClientMultiComptes("Bob","Eponge", _accountTen);
		System.out.printf("Solde : %.1f\n", _client.getSolde());
		
		Compte _accountTweety = new Compte(20);
		_accountTweety.Depot(2500);	
		_client.AjouterCompte(_accountTweety);
		System.out.printf("Solde : %.1f\n", _client.getSolde());
		
		_client.AfficherEtatClient();
		
		//Ex 5.2
		System.out.println("------ S�nario de l'exercice 5.2 ------");
		Compte _cp1 = new Compte (1);
		_cp1.Depot(1000);
		Compte _cp2 = new Compte (2);
		System.out.println(_cp1.Virer(_cp2, 600));
		System.out.printf("Compte cp1 : %.1f - cp2 %.1f\n", _cp1.getSolde(), _cp2.getSolde());
		System.out.println(_cp1.Virer(_cp2, 600));
		System.out.printf("Compte cp1 : %.1f - cp2 %.1f\n", _cp1.getSolde(), _cp2.getSolde());
		
	}
}
