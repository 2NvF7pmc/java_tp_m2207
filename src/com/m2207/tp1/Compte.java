package com.m2207.tp1;

public class Compte
{
	private int numero;
	private double solde, decouvert;
	
	
	public int getNumero() { return numero; }
	
	public double getSolde() { return solde; }
	
	public void setDecouvert(double _montant) { decouvert = _montant; }
	public double getDecouvert() { return decouvert; }
	
	public Compte (int _numero)
	{
		numero = _numero;
		
		solde = 0;
		decouvert = 0;
	}
	
	public void AfficherSolde()
	{
		System.out.printf("Solde : %.1f\n", solde);
	}
	
	public void Depot (double _montant)
	{
		solde += _montant;
	}
	
	public String Retrait (double _montant)
	{
		if(_montant <= (solde + decouvert))
		{
			solde -= _montant;
			return "Retrait effectu�.";
		}
		
		return "Retrait refus�.";
	}
	
	/*
	 * Permet de faire des virements entre compte
	 */
	public String Virer (Compte _destinataire, double _montant)
	{
		if(_montant <= solde)
		{
			solde -= _montant;
			_destinataire.Depot(_montant);
			return "Virement effectu�.";
		}
		
		return "Virement Refus�.";
	}
	
}
