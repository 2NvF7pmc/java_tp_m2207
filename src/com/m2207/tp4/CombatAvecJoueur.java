package com.m2207.tp4;

import java.util.Scanner;

public class CombatAvecJoueur
{
    private static Scanner Input = new Scanner(System.in);
    private static Pokemon Combatants[] = new Pokemon[2];

    public static void main(String[] args)
    {
        for (int i = 0; i < Combatants.length; i++)
        {
            System.out.println("Saisir le nom du premier combattant");
            String _name = Input.nextLine();
            Combatants[i] = new Pokemon(_name);
        }
        printLine();
        startCombat ();
    }

    private static void startCombat ()
    {
        boolean _invert = false;
        int _rounds = 0;
        while (true)
        {
            _rounds++;
            System.out.printf("Round %d\n", _rounds);
            System.out.printf("Etat des combattants : %s : (en)%d (atk)%d / %s : (en)%d (atk)%d\n",
                    Combatants[0].getNom(),
                    Combatants[0].getEnergie(),
                    Combatants[0].getPuissance(),
                    Combatants[1].getNom(),
                    Combatants[1].getEnergie(),
                    Combatants[1].getPuissance());

            boolean _end = true;
            int _cmd = -999;
            do {
                switch (_cmd)
                {
                    case 0:
                        if(_invert) Combatants[0].attaquer(Combatants[1]);
                        else Combatants[1].attaquer(Combatants[0]);
                        _end = false;
                        break;

                    case 1:
                        if(_invert) Combatants[0].manger();
                        else Combatants[1].manger();
                        _end = false;
                        break;

                    default:
                        System.out.printf("%s : Attaquer(0) ou Manger(1) ?",
                                ((_invert) ? Combatants[0].getNom() : Combatants[1].getNom()));
                        _cmd = Input.nextInt();
                        break;
                }
            } while (_end);
            _invert = !_invert;

            if(!Combatants[0].isAlive())
            {
                printLine();
                System.out.printf("%s gagne en %d rounds.\n",
                        Combatants[1].getNom(),
                        _rounds);
                break;
            }
            else if(!Combatants[1].isAlive())
            {
                printLine();
                System.out.printf("%s gagne en %d rounds.\n",
                        Combatants[0].getNom(),
                        _rounds);
                break;
            }
        }
    }

    private static void printLine ()
    {
        System.out.println("--------------------------------------");
    }
}
