package com.m2207.tp4;

import java.util.Vector;

public class Joueur
{
    private String Name;
    public Vector<Pokemon> Combatants;

    public String getName() { return Name; }

    public void addCombatants(Pokemon _newCombatants)
    {
        Combatants.add(_newCombatants);
    }

    public Joueur (String _name)
    {
        Name = _name;
        Combatants = new Vector<Pokemon>();
    }

    public String pokemonResume ()
    {
        String _result = "Etat des combattants : ";
        for (Pokemon _p: Combatants)
            _result += String.format("%s : (en)%d (atk)%d, ", _p.getNom(), _p.getEnergie(), _p.getPuissance());

        return _result;
    }
}
