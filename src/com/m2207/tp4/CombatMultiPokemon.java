package com.m2207.tp4;

import java.util.Scanner;
import java.util.Vector;

public class CombatMultiPokemon
{
    private static final int  NBR_POK_PAR_JOUEUR = 2;
    private static final int NBR_JOUEUR = 2;

    private static Scanner Input = new Scanner(System.in);
    private static Joueur[] Players = new Joueur[NBR_JOUEUR];

    public static void main(String[] args)
    {
        for (int j = 0; j < NBR_JOUEUR; j++)
        {
            System.out.println("Saisir le nom du joueur >");
            String _playerName = Input.nextLine();

            Players[j] = new Joueur(_playerName);

            for (int i = 0; i < NBR_POK_PAR_JOUEUR; i++)
            {
                System.out.printf("Saisir le nom du pokemon n°%d >\n", i);
                String _pokName = Input.nextLine();
                Players[j].addCombatants(new Pokemon(_pokName));
            }
        }
        startCombat();
    }

    //A chaque tour, le joueur joue un par un tous ses Pokemons encore vivants.
    private static void startCombat ()
    {
        boolean _invert = false;
        int _rounds = 0;
        while (true)
        {
            _rounds++;

            //Info
            System.out.printf("Round %d\n", _rounds);
            PrintLine();
            resume();
            PrintLine();

            if(_invert) System.out.printf("Joueur %s c'est votre tour : \n", Players[0].getName());
            else System.out.printf("Joueur %s c'est votre tour : \n", Players[1].getName());

            for (int i = 0; i < NBR_POK_PAR_JOUEUR; i++)
            {
                int _playerId = (_invert) ? 0 : 1;
                int _t = ((_invert) ? 1 : 0);
                if(Players[_playerId].Combatants.get(i).isAlive())
                {
                    boolean _end = true;
                    int _cmd = -999;
                    do {
                        switch (_cmd)
                        {
                            //Attaquer
                            case 0:
                                getTarget(_t, Players[_playerId].Combatants.get(i));
                                _end = false;
                                break;

                            //Manger
                            case 1:
                                Players[_playerId].Combatants.get(i).manger();
                                _end = false;
                                break;

                            default:
                                System.out.printf("Pokemon %s - Attaquer(0) ou Manger(1) ?\n",
                                        Players[0].Combatants.get(i).getNom(),
                                        Players[_playerId].Combatants.get(i).getNom());
                                _cmd = Input.nextInt();
                                break;
                        }
                    } while (_end);
                }
            }

            PrintLine();
            _invert = !_invert;
        }
    }

    /*
    * Fonction permet de choisir le pokemon a combatre
     */
    private static void getTarget (int _playerTargetId, Pokemon _attaquant)
    {
        int _cmd = -999;
        while (true)
        {
            if(_cmd >= 0 && Players[_playerTargetId].Combatants.size() > _cmd && Players[_playerTargetId].Combatants.get(_cmd).isAlive())
            {
                Players[_playerTargetId].Combatants.get(_cmd).attaquer(_attaquant);
                System.out.printf("%s attaque %s\n", _attaquant.getNom(), Players[_playerTargetId].Combatants.get(_cmd).getNom());
                break;
            }
            else
            {
                System.out.println("Quel pokemon voulais vous ataquer >");
                _cmd = Input.nextInt();
            }
        }
    }

    private static void resume ()
    {
        for (Joueur _j : Players)
            System.out.printf("Joueur %s - %s\n", _j.getName(), _j.pokemonResume());
    }

    private static void PrintLine ()
    {
        System.out.println("-----------------------------------------");
    }
}