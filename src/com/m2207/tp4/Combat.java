package com.m2207.tp4;

public class Combat
{
    private static final int NbrCombatant = 2;
    private static Pokemon[] Combatants = new Pokemon[NbrCombatant];

    public static void main(String[] args)
    {
        Combatants[0] = new Pokemon("Pok&Null");
        Combatants[1] = new Pokemon("PokIsBad");

        for (Pokemon _p : Combatants) _p.sePresenter();

        PrintLine ();
        boolean _invert = false;
        boolean _roundState = true;
        int _rounds = 0;
        while (_roundState)
        {
            _rounds++;
            System.out.printf("Round %d - %s : (en)%d (atk)%d - %s : (en)%d (atk)%d\n",
                    _rounds,
                    Combatants[0].getNom(),
                    Combatants[0].getEnergie(),
                    Combatants[0].getPuissance(),
                    Combatants[1].getNom(),
                    Combatants[1].getEnergie(),
                    Combatants[1].getPuissance());

            //Attaquer
            if(_invert) Combatants[0].attaquer(Combatants[1]);
            else Combatants[1].attaquer(Combatants[0]);
            _invert = !_invert;

            for (Pokemon _p : Combatants)
            {
                if(!_p.isAlive())
                {
                    PrintLine ();
                    System.out.printf("%s gagne en %d rounds\n",
                            _p.getNom(),
                            _rounds);
                    _roundState = false;
                }
            }
        }
    }

    private static void PrintLine ()
    {
        System.out.println("-----------------------------------------");
    }
}
